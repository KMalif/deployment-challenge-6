package com.challenge.api_ticket_reservation.Controller;

import com.challenge.api_ticket_reservation.Dto.UsersDto;
import com.challenge.api_ticket_reservation.Entity.Users;
import com.challenge.api_ticket_reservation.Service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController {
    @Autowired
    UserService userService;

    @PostMapping("api/user")
    public Users postUserCtrl(@RequestBody UsersDto usersDto){
        return userService.register(usersDto);
    }

    @GetMapping("api/users")
    public List<Users> getAllUsersCtrl(){
        return userService.getAllUsers();
    }

    @GetMapping("api/users/{id}")
    public Users getUserByIdCtrl(@PathVariable(name = "id") int id){
        return userService.getUserById(id);
    }

    @PutMapping("api/users/{id}")
    public Users updateUserCtrl(@PathVariable(name = "id") int id, @RequestBody UsersDto usersDto){
        return userService.updateUser(id, usersDto);
    }

    @DeleteMapping("api/users/{id}")
    public void deleteUserCtrl(@PathVariable(name = "id") int id){
        userService.deleteUser(id);
    }

}
