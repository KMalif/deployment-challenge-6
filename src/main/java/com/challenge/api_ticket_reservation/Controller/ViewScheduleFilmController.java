package com.challenge.api_ticket_reservation.Controller;

import com.challenge.api_ticket_reservation.Service.ViewScheduleDetailServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ViewScheduleFilmController {
    @Autowired
    ViewScheduleDetailServices viewScheduleDetailServices;

    @GetMapping("api/schedules/detail")
    public ResponseEntity<?> getAllDetailScheduleCtrl(){
        return new ResponseEntity<>(viewScheduleDetailServices.getAllScheduleDetail(), HttpStatus.ACCEPTED);
    }

}
