package com.challenge.api_ticket_reservation.Controller;

import com.challenge.api_ticket_reservation.Entity.MasterSeats;
import com.challenge.api_ticket_reservation.Service.MasterSeatsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MasterSeatsController {
    @Autowired
    MasterSeatsServices masterSeatsServices;

    @GetMapping("api/seats/")
    public ResponseEntity<?> getSeatByStatusCtrl(@RequestParam(value = "status") boolean status){
        List<MasterSeats> seatsStatus = masterSeatsServices.getStudioByStatus(status);
        return new ResponseEntity<>(seatsStatus, HttpStatus.ACCEPTED);
    }
}
