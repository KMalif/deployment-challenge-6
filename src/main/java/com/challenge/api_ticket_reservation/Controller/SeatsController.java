package com.challenge.api_ticket_reservation.Controller;

import com.challenge.api_ticket_reservation.Dto.SeatsDto;
import com.challenge.api_ticket_reservation.Entity.Seats;
import com.challenge.api_ticket_reservation.Repository.SeatsRepository;
import com.challenge.api_ticket_reservation.Service.SeatsServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class SeatsController {
    @Autowired
    SeatsServices seatsServices;

    @PostMapping("api/seats")
    public Map<String, Object> postSeatsCtrl(@RequestBody SeatsDto seatsDto){
        Seats seats = seatsServices.postSeat(seatsDto);
        Map<String, Object> map = new HashMap<>();
        map.put("studio", seatsDto);
        map.put("detail_studio", seatsDto.getDetailSeats());
        return map;
    }
}
