package com.challenge.api_ticket_reservation.Controller;

import com.challenge.api_ticket_reservation.Dto.CustomerOrderDto;
import com.challenge.api_ticket_reservation.Service.CustomerOrderServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerOrderController {
    @Autowired
    CustomerOrderServices customerOrderServices;


    @PostMapping("api/order")
    public ResponseEntity<?> postCustomerOrderCtrl(@RequestBody CustomerOrderDto customerOrderDto){
        customerOrderServices.postCustomerOrder(customerOrderDto);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
