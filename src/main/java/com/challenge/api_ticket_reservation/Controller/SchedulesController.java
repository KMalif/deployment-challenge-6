package com.challenge.api_ticket_reservation.Controller;

import com.challenge.api_ticket_reservation.Dto.SchedulesDto;
import com.challenge.api_ticket_reservation.Entity.Schedules;
import com.challenge.api_ticket_reservation.Service.interfaces.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SchedulesController {
    @Autowired
    ScheduleService scheduleService;

    @PostMapping("api/schedule")
    public Schedules postScheduleCtrl(@RequestBody SchedulesDto schedulesDto){
        return scheduleService.postSchedule(schedulesDto);
    }

    @GetMapping("api/schedules")
    public List<Schedules> getAllSchedulesCtrl(){
        return scheduleService.getAllSchedules();
    }
}
