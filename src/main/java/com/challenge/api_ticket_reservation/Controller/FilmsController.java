package com.challenge.api_ticket_reservation.Controller;

import com.challenge.api_ticket_reservation.Dto.FilmsDto;
import com.challenge.api_ticket_reservation.Entity.Films;
import com.challenge.api_ticket_reservation.Service.interfaces.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FilmsController {
    @Autowired
    FilmService filmService;

    @PostMapping("api/films")
    public Films postFilmCtrl(@RequestBody FilmsDto filmsDto){
        return filmService.post_films(filmsDto);
    }

    @GetMapping("api/films")
    public List<Films> getAllFilmsCtrl(){
        return filmService.getAllFilms();
    }

    @GetMapping("api/films/{id}")
    public Films getFilmsByIdCtrl(@PathVariable(name = "id") int id){
        return filmService.getFilmById(id);
    }

    @GetMapping("api/films/")
    public List<Films> getFilmByShowedCtrl(@RequestParam boolean showed){
        return filmService.getShowedFilms(showed);
    }

    @PutMapping("api/films/{id}")
    public Films updateFilmCtrl(@PathVariable(name = "id") int id, @RequestBody FilmsDto filmsDto){
        return filmService.updateFilm(id, filmsDto);
    }

    @DeleteMapping("api/films/{id}")
    public void deleteFilmCtrl(@PathVariable(name = "id") int id){
        filmService.deleteFilm(id);
    }
}
