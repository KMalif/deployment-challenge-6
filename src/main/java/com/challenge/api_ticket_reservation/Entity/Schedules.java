package com.challenge.api_ticket_reservation.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Schedules")
public class Schedules {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_schedules")
    private int idSchedules;


    @Column(name = "show_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date showDate;

    @Column(name = "start")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date start;

    @Column(name = "end")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date end;

    @Column(name = "ticket_price")
    private int ticketPrice;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "films_id", nullable = false, referencedColumnName = "id_films")
    private Films films;

    public int getIdSchedules() {
        return idSchedules;
    }

    public void setIdSchedules(int idSchedules) {
        this.idSchedules = idSchedules;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public Films getFilms() {
        return films;
    }

    public void setFilms(Films films) {
        this.films = films;
    }
}
