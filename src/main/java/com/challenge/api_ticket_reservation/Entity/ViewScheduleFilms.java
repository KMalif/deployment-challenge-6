package com.challenge.api_ticket_reservation.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "vw_schedule_film_detail")
public class ViewScheduleFilms {
    @Id
    @Column(name = "schedules_id")
    private int schedulesId;

    @Column(name = "show_date")
    private Date showDate;

    @Column(name= "film_name")
    private String filmName;

    @Column(name = "id_films")
    private int idFilms;

    @Column(name = "ticket_price")
    private BigDecimal ticketPrice;

    @Column(name = "studio_name")
    private String studioName;

    @Column(name = "seat_number")
    private int seatNumber;

    public int getSchedulesId() {
        return schedulesId;
    }

    public void setSchedulesId(int schedulesId) {
        this.schedulesId = schedulesId;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public int getIdFilms() {
        return idFilms;
    }

    public void setIdFilms(int idFilms) {
        this.idFilms = idFilms;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }
}
