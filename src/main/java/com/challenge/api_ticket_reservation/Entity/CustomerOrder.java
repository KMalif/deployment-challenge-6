package com.challenge.api_ticket_reservation.Entity;

import javax.persistence.*;

@Entity
@Table(name = "customer_order")
public class CustomerOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ticket")
    private int idTicket;

    @Column(name = "id_seats")
    private int idSeats;

    @Column(name = "film_code")
    private int filmCode;

    @Column(name = "id_schedules")
    private int idSchedules;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_users", nullable = false)
    private Users idUser;

    public int getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(int idTicket) {
        this.idTicket = idTicket;
    }

    public int getIdSeats() {
        return idSeats;
    }

    public void setIdSeats(int idSeats) {
        this.idSeats = idSeats;
    }

    public int getFilmCode() {
        return filmCode;
    }

    public void setFilmCode(int filmCode) {
        this.filmCode = filmCode;
    }

    public int getIdSchedules() {
        return idSchedules;
    }

    public void setIdSchedules(int idSchedules) {
        this.idSchedules = idSchedules;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }
}
