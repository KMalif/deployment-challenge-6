package com.challenge.api_ticket_reservation.Entity;

import com.challenge.api_ticket_reservation.Utils.RoleEnums;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Users")
@Setter
@Getter
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_users")
    private int idUsers;

    @Column(name = "username",unique = true )
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(length = 10, columnDefinition = "varchar(10) default 'CUSTOMER'")
    @Enumerated(EnumType.STRING)
    private RoleEnums roleEnums = RoleEnums.CUSTOMER;



}
