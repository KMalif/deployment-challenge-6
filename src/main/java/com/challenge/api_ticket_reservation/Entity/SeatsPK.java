package com.challenge.api_ticket_reservation.Entity;

import java.io.Serializable;

public class SeatsPK implements Serializable {
    private int idFilms;
    private int idSchedules;

    public int getIdFilms() {
        return idFilms;
    }

    public void setIdFilms(int idFilms) {
        this.idFilms = idFilms;
    }

    public int getIdSchedules() {
        return idSchedules;
    }

    public void setIdSchedules(int idSchedules) {
        this.idSchedules = idSchedules;
    }
}
