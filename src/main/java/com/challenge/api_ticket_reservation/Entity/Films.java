package com.challenge.api_ticket_reservation.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Films")
@Setter
@Getter
@JsonIgnoreProperties(value = {"hibernateLazyInitializer","handler"})
public class Films {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_films")
    private int idFilms;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "showed")
    private boolean showed;

    public Films (){}

    public Films(int idFilms, String filmName, boolean showed) {
        this.idFilms = idFilms;
        this.filmName = filmName;
        this.showed = showed;
    }


}
