package com.challenge.api_ticket_reservation.Entity;

import javax.persistence.*;

@Entity
@Table(name = "seats")
public class Seats {
    @EmbeddedId
    @AttributeOverrides
            ({
                    @AttributeOverride(name = "idFilms", column = @Column(name = "film_id")),
                    @AttributeOverride(name = "idSchedules", column = @Column(name = "schedules_id"))

            })
    private SeatsPK seatsId;

    @Column(name = "studio_name")
    private String studioName;

    @Column(name = "seat_number")
    private int seat_number;

    public SeatsPK getSeatsId() {
        return seatsId;
    }

    public void setSeatsId(SeatsPK seatsId) {
        this.seatsId = seatsId;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public int getSeat_number() {
        return seat_number;
    }

    public void setSeat_number(int seat_number) {
        this.seat_number = seat_number;
    }
}
