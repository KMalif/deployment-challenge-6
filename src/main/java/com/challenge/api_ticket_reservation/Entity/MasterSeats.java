package com.challenge.api_ticket_reservation.Entity;

import javax.persistence.*;

@Entity
@Table(name = "master_studio")
public class MasterSeats {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_studio")
    private int idStudio;

    @Column(name = "studio_name")
    private String studioName;

    @Column(name = "seats_number")
    private int seatsNumber;

    @Column(name = "status")
    private boolean status;

    public int getIdStudio() {
        return idStudio;
    }

    public void setIdStudio(int idStudio) {
        this.idStudio = idStudio;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
