package com.challenge.api_ticket_reservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTicketReservationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTicketReservationApplication.class, args);
	}
}
