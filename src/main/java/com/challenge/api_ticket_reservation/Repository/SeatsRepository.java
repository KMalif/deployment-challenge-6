package com.challenge.api_ticket_reservation.Repository;

import com.challenge.api_ticket_reservation.Entity.Seats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface SeatsRepository extends JpaRepository<Seats, Integer> {
    public List<Seats> findAll();
}
