package com.challenge.api_ticket_reservation.Repository;

import com.challenge.api_ticket_reservation.Entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface UsersRepository extends JpaRepository<Users,Integer> {
    Users findById(int id);
    Users findByUsername(String username);
}
