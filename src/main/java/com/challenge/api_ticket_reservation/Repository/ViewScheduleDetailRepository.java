package com.challenge.api_ticket_reservation.Repository;

import com.challenge.api_ticket_reservation.Entity.ViewScheduleFilms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ViewScheduleDetailRepository extends JpaRepository<ViewScheduleFilms, Integer> {
    public List<ViewScheduleFilms> findAll();

}
