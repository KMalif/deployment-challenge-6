package com.challenge.api_ticket_reservation.Repository;

import com.challenge.api_ticket_reservation.Entity.MasterSeats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.beans.Transient;
import java.util.List;

@Repository
@Transactional
public interface MasterSeatsRepository extends JpaRepository<MasterSeats, Integer> {
    public List<MasterSeats> findByStatus(boolean status);
    public MasterSeats findByIdStudio(int idStudio);
}
