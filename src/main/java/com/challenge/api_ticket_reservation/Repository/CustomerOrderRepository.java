package com.challenge.api_ticket_reservation.Repository;

import com.challenge.api_ticket_reservation.Entity.CustomerOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface CustomerOrderRepository extends JpaRepository<CustomerOrder, Integer> {

}
