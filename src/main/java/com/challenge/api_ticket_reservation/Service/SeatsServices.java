package com.challenge.api_ticket_reservation.Service;

import com.challenge.api_ticket_reservation.Dto.SeatsDto;
import com.challenge.api_ticket_reservation.Entity.MasterSeats;
import com.challenge.api_ticket_reservation.Entity.Seats;
import com.challenge.api_ticket_reservation.Repository.MasterSeatsRepository;
import com.challenge.api_ticket_reservation.Repository.SeatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeatsServices {
    @Autowired
    SeatsRepository seatsRepository;

    @Autowired
    MasterSeatsRepository masterSeatsRepository;

    public Seats postSeat(SeatsDto seatsDto){
        Seats seats = new Seats();
        MasterSeats updateSeatsStatus  = masterSeatsRepository.findByIdStudio(seatsDto.getDetailSeats().getIdStudio());
        seatsDto.getDetailSeats().setNoSeat(updateSeatsStatus.getSeatsNumber());
        seatsDto.getDetailSeats().setStudioName(updateSeatsStatus.getStudioName());
        seats.setSeatsId(seatsDto.getSeatsId());
        seats.setStudioName(seatsDto.getStudioName());
        seats.setSeat_number(seats.getSeat_number());
        return seatsRepository.save(seats);
    }
}
