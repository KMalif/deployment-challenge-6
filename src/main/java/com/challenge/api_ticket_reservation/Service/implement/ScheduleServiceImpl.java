package com.challenge.api_ticket_reservation.Service.implement;

import com.challenge.api_ticket_reservation.Dto.SchedulesDto;
import com.challenge.api_ticket_reservation.Entity.Schedules;
import com.challenge.api_ticket_reservation.Repository.SchedulesRepository;
import com.challenge.api_ticket_reservation.Service.interfaces.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    SchedulesRepository schedulesRepository;

    @Override
    public Schedules postSchedule(SchedulesDto schedulesDto) {
        Schedules schedules = new Schedules();
        schedules.setShowDate(schedulesDto.getShowDate());
        schedules.setStart(schedulesDto.getStart());
        schedules.setEnd(schedulesDto.getEnd());
        schedules.setTicketPrice(schedulesDto.getTicketPrice());
        schedules.setFilms(schedulesDto.getFilms());
        return schedulesRepository.save(schedules);
    }

    @Override
    public List<Schedules> getAllSchedules() {

        return schedulesRepository.findAll();
    }
}
