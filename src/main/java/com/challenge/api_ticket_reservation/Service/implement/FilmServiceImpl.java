package com.challenge.api_ticket_reservation.Service.implement;

import com.challenge.api_ticket_reservation.Dto.FilmsDto;
import com.challenge.api_ticket_reservation.Entity.Films;
import com.challenge.api_ticket_reservation.Repository.FilmsRepository;
import com.challenge.api_ticket_reservation.Service.interfaces.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmServiceImpl implements FilmService {
    @Autowired
    FilmsRepository filmsRepository;

    @Override
    public Films post_films(FilmsDto filmsDto) {
        Films films = new Films();
        films.setFilmName(filmsDto.getFilmName());
        films.setShowed(filmsDto.isShowed());
        return filmsRepository.save(films);
    }

    @Override
    public List<Films> getShowedFilms(boolean showed) {
        return filmsRepository.findByShowed(showed);
    }

    @Override
    public List<Films> getAllFilms() {
        return filmsRepository.findAll();
    }

    @Override
    public Films getFilmById(int id) {
        try{
            Films films = filmsRepository.findById(id);
            if (films != null){
                filmsRepository.findById(id);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Film not found");
        }
        return filmsRepository.findById(id);
    }

    @Override
    public Films updateFilm(int id, FilmsDto filmsDto) {
        Films filmUpdate = new Films();
        try {
            Films films = filmsRepository.findById(id);
            if (films != null){
                films.setFilmName(filmsDto.getFilmName());
                films.setShowed(filmsDto.isShowed());
                filmUpdate = filmsRepository.save(films);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Film not found");
        }
        return filmUpdate;
    }

    @Override
    public void deleteFilm(int id) {
        try{
            Films films = filmsRepository.findById(id);
            if (films != null){
                filmsRepository.deleteById(id);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Film not found");
        }
    }
}
