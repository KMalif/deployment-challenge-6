package com.challenge.api_ticket_reservation.Service.implement;

import com.challenge.api_ticket_reservation.Dto.UsersDto;
import com.challenge.api_ticket_reservation.Entity.Users;
import com.challenge.api_ticket_reservation.Repository.UsersRepository;
import com.challenge.api_ticket_reservation.Service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Users register(UsersDto usersDto) {
        String encode = bCryptPasswordEncoder.encode(usersDto.getPassword());
        Users users = new Users();
        users.setUsername(usersDto.getUsername());
        users.setEmail(usersDto.getEmail());
        users.setPassword(encode);
        users.setFirstName(usersDto.getFirstName());
        users.setLastName(usersDto.getLastName());
        if (usersDto.getRoleEnums() != null){
            users.setRoleEnums(usersDto.getRoleEnums());
        }
        return usersRepository.save(users);
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public Users getUserById(int id) {
        try{
            Users users = usersRepository.findById(id);
            if (users != null){
                usersRepository.findById(id);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("User not found");
        }
        return usersRepository.findById(id);
    }

    @Override
    public Users updateUser(int id, UsersDto usersDto) {
        Users usersUpdate = new Users();

        try{
            Users users = usersRepository.findById(id);
            if (users != null){
                users.setUsername(usersDto.getUsername());
                users.setEmail(usersDto.getEmail());
                users.setPassword(usersDto.getPassword());
                usersUpdate = usersRepository.save(users);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("User not found");
        }
        return usersUpdate;
    }

    @Override
    public void deleteUser(int id) {
        try{
            Users users = usersRepository.findById(id);
            if (users != null){
                usersRepository.deleteById(id);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("User not found");
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = usersRepository.findByUsername(username);
        if(users != null){
            log.info("User found in the database : {}", username);
        }else{
            log.error("User not found in the database");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(users.getRoleEnums().name()));
        return new org.springframework.security.core.userdetails.User(users.getUsername(),users.getPassword(), authorities);
    }
}
