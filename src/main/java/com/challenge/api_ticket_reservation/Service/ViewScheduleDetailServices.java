package com.challenge.api_ticket_reservation.Service;

import com.challenge.api_ticket_reservation.Entity.ViewScheduleFilms;
import com.challenge.api_ticket_reservation.Repository.ViewScheduleDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ViewScheduleDetailServices {
    @Autowired
    ViewScheduleDetailRepository viewScheduleDetailRepository;

    public List<ViewScheduleFilms> getAllScheduleDetail(){
        return viewScheduleDetailRepository.findAll();
    }
}
