package com.challenge.api_ticket_reservation.Service;

import com.challenge.api_ticket_reservation.Dto.CustomerOrderDto;
import com.challenge.api_ticket_reservation.Entity.*;
import com.challenge.api_ticket_reservation.Repository.CustomerOrderRepository;
import com.challenge.api_ticket_reservation.Repository.FilmsRepository;
import com.challenge.api_ticket_reservation.Repository.SchedulesRepository;
import com.challenge.api_ticket_reservation.Repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerOrderServices {
    @Autowired
    CustomerOrderRepository customerOrderRepository;

    @Autowired
    SchedulesRepository schedulesRepository;

    @Autowired
    FilmsRepository filmsRepository;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    MasterSeatsServices masterSeatsServices;

    public void postCustomerOrder(CustomerOrderDto customerOrderDto){
        CustomerOrder customerOrder = new CustomerOrder();
        try{
            Schedules schedules = schedulesRepository.findById(customerOrderDto.getIdSchedules());
            Films films = filmsRepository.findById(customerOrderDto.getFilmCode());
            Users users = usersRepository.findById(customerOrderDto.getIdUser().getIdUsers());

            MasterSeats masterSeats = masterSeatsServices.updateSeats(customerOrderDto.getIdSeats(), customerOrderDto.isStatus());
            customerOrder.setIdSeats(customerOrderDto.getIdSeats());
            customerOrder.setFilmCode(customerOrderDto.getFilmCode());
            customerOrder.setIdSchedules(customerOrderDto.getIdSchedules());
            customerOrder.setIdUser(customerOrderDto.getIdUser());
            customerOrderRepository.save(customerOrder);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
