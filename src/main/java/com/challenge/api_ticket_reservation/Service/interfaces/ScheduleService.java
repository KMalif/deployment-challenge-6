package com.challenge.api_ticket_reservation.Service.interfaces;

import com.challenge.api_ticket_reservation.Dto.SchedulesDto;
import com.challenge.api_ticket_reservation.Entity.Schedules;
import com.challenge.api_ticket_reservation.Repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScheduleService {

    public Schedules postSchedule(SchedulesDto schedulesDto);

    public List<Schedules> getAllSchedules();
}
