package com.challenge.api_ticket_reservation.Service.interfaces;

import com.challenge.api_ticket_reservation.Dto.UsersDto;
import com.challenge.api_ticket_reservation.Entity.Users;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    public Users register(UsersDto usersDto);
    public List<Users> getAllUsers();
    public Users getUserById(int id);
    public Users updateUser(int id, UsersDto usersDto);
    public void deleteUser(int id);

}
