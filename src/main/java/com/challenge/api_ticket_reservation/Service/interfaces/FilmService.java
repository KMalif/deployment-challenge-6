package com.challenge.api_ticket_reservation.Service.interfaces;

import com.challenge.api_ticket_reservation.Dto.FilmsDto;
import com.challenge.api_ticket_reservation.Entity.Films;

import java.util.List;

public interface FilmService {
    public Films post_films(FilmsDto filmsDto);
    public List<Films> getAllFilms();
    public List<Films> getShowedFilms(boolean showed);
    public Films getFilmById(int id);
    public Films updateFilm(int id, FilmsDto filmsDto);
    public void deleteFilm(int id);
}
