package com.challenge.api_ticket_reservation.Service;

import com.challenge.api_ticket_reservation.Entity.MasterSeats;
import com.challenge.api_ticket_reservation.Repository.MasterSeatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MasterSeatsServices {
    @Autowired
    MasterSeatsRepository masterSeatsRepository;

    public List<MasterSeats> getStudioByStatus (boolean status){
        return masterSeatsRepository.findByStatus(status);
    }

    public MasterSeats updateSeats(int idStudio, boolean status){
        MasterSeats masterSeats = masterSeatsRepository.findByIdStudio(idStudio);

        try{
            if (masterSeats != null){
                masterSeats.setStatus(status);
                return masterSeatsRepository.save(masterSeats);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return masterSeats;
    }

}
