package com.challenge.api_ticket_reservation.Dto;

import com.challenge.api_ticket_reservation.Utils.RoleEnums;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UsersDto {
    private int idUsers;
    private String username;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private RoleEnums roleEnums;


}
