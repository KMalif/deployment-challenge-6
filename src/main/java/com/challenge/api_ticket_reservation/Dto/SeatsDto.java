package com.challenge.api_ticket_reservation.Dto;

import com.challenge.api_ticket_reservation.Entity.SeatsPK;


public class SeatsDto {
    private SeatsPK seatsId;
    private int seat_number;
    private String studioName;

    private MasterSeatsDto detailSeats;


    public SeatsPK getSeatsId() {
        return seatsId;
    }

    public void setSeatsId(SeatsPK seatsId) {
        this.seatsId = seatsId;
    }

    public int getSeat_number() {
        return seat_number;
    }

    public void setSeat_number(int seat_number) {
        this.seat_number = seat_number;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public MasterSeatsDto getDetailSeats() {
        return detailSeats;
    }

    public void setDetailSeats(MasterSeatsDto detailSeats) {
        this.detailSeats = detailSeats;
    }
}
