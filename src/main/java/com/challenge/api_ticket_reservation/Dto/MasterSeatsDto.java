package com.challenge.api_ticket_reservation.Dto;


public class MasterSeatsDto {
    private int idStudio;
    private String studioName;
    private int seatsNumber;
    private boolean status;


    public int getIdStudio() {
        return idStudio;
    }

    public void setIdStudio(int idStudio) {
        this.idStudio = idStudio;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public int getNoSeat() {
        return seatsNumber;
    }

    public void setNoSeat(int noSeat) {
        this.seatsNumber = noSeat;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
