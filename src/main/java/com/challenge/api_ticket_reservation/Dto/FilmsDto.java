package com.challenge.api_ticket_reservation.Dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FilmsDto {
    private int idFilms;
    private String filmName;
    private boolean showed;

}
