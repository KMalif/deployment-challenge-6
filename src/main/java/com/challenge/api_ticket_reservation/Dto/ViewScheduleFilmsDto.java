package com.challenge.api_ticket_reservation.Dto;

import java.math.BigDecimal;
import java.util.Date;

public class ViewScheduleFilmsDto {

    private int schedulesId;

    private Date showDate;

    private String filmName;

    private int idFilms;

    private BigDecimal ticketPrice;

    private String studioName;

    private int seatNumber;

    public int getSchedulesId() {
        return schedulesId;
    }

    public void setSchedulesId(int schedulesId) {
        this.schedulesId = schedulesId;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public int getIdFilms() {
        return idFilms;
    }

    public void setIdFilms(int idFilms) {
        this.idFilms = idFilms;
    }

    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getStudioName() {
        return studioName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }
}
