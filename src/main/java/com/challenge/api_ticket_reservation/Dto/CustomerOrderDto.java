package com.challenge.api_ticket_reservation.Dto;

import com.challenge.api_ticket_reservation.Entity.Users;


public class CustomerOrderDto {
    private int idTicket;

    private int idSeats;

    private int filmCode;

    private int idSchedules;

    private Users idUser;

    private boolean status;

    public int getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(int idTicket) {
        this.idTicket = idTicket;
    }

    public int getIdSeats() {
        return idSeats;
    }

    public void setIdSeats(int idSeats) {
        this.idSeats = idSeats;
    }

    public int getFilmCode() {
        return filmCode;
    }

    public void setFilmCode(int filmCode) {
        this.filmCode = filmCode;
    }

    public int getIdSchedules() {
        return idSchedules;
    }

    public void setIdSchedules(int idSchedules) {
        this.idSchedules = idSchedules;
    }

    public Users getIdUser() {
        return idUser;
    }

    public void setIdUser(Users idUser) {
        this.idUser = idUser;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
