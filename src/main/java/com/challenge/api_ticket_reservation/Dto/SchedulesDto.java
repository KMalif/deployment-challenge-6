package com.challenge.api_ticket_reservation.Dto;

import com.challenge.api_ticket_reservation.Entity.Films;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class SchedulesDto {
    private int idSchedules;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date showDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date start;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date end;
    private int ticketPrice;
    private Films films;

    public int getIdSchedules() {
        return idSchedules;
    }

    public void setIdSchedules(int idSchedules) {
        this.idSchedules = idSchedules;
    }

    public Date getShowDate() {
        return showDate;
    }

    public void setShowDate(Date showDate) {
        this.showDate = showDate;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public Films getFilms() {
        return films;
    }

    public void setFilms(Films films) {
        this.films = films;
    }
}
